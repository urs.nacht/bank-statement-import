# Copyright (C) 2013-2015 Therp BV <http://therp.nl>

{
    'name': 'MT940 Bank Statements Import',
    'version': '1.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Flectra Community, Odoo Community Association (OCA), Therp BV',
    'website': 'https://gitlab.com/flectra-community/bank-statement-import',
    'category': 'Banking addons',
    'depends': [
        'account_bank_statement_import',
    ],
    'installable': True
}

*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: https://www.gnu.com/licenses/agpl
    :alt: License: AGPL-3

=========================
Bank Statement Parse Camt
=========================

Module to import SEPA CAMT.053 and CAMT.054 Format bank statement files.

Based on the Banking addons framework.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Holger Brunn <hbrunn@therp.nl>
* Stefan Rijnhart <srijnhart@therp.nl>
* Ronald Portier <rportier@therp.nl>
* Andrea Stirpe <a.stirpe@onestein.nl>
* Maxence Groine <mgroine@fiefmanage.ch>

Do not contact contributors directly about support or help with technical issues.
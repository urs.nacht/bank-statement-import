# Flectra Community / bank-statement-import

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_bank_statement_import_move_line](account_bank_statement_import_move_line/) | 1.0.1.0.0| Import journal items into bank statement
[account_bank_statement_import_camt_oca](account_bank_statement_import_camt_oca/) | 1.0.1.0.3| CAMT Format Bank Statements Import
[account_bank_statement_import_paypal](account_bank_statement_import_paypal/) | 1.0.1.0.0| Import Paypal CSV files as Bank Statements in Odoo
[account_bank_statement_import_mt940_base](account_bank_statement_import_mt940_base/) | 1.0.1.0.0| MT940 Bank Statements Import
[account_bank_statement_import_ofx](account_bank_statement_import_ofx/) | 1.0.1.0.0| Import OFX Bank Statement
[account_bank_statement_import_qif](account_bank_statement_import_qif/) | 1.0.1.0.1| Import QIF Bank Statements



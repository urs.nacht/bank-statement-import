*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

==========================
Import QIF bank statements
==========================

This module allows you to import the machine readable QIF Files in Odoo: they
are parsed and stored in human readable format in
Accounting \ Bank and Cash \ Bank Statements.

Important Note
--------------
Because of the QIF format limitation, we cannot ensure the same transactions
aren't imported several times or handle multicurrency. Whenever possible, you
should use a more appropriate file format like OFX.

The module was initiated as a backport of the new framework developed
by Odoo for V9 at its early stage. As Odoo has relicensed this module as
private inside its Odoo enterprise layer, now this one is maintained from the
original AGPL code.

Usage
=====

To use this module, you need to:

#. Go to *Invoicing / Accounting* dashboard.
#. Click on *Import statement* from any of the bank journals.
#. Select a QIF file.
#. Press *Import*.

Credits
=======

Contributors
------------    

* Odoo SA
* Akretion
  * Alexis de Lattre <alexis@via.ecp.fr>
* ACSONE A/V
  * Laurent Mignon <laurent.mignon@acsone.eu>
* Therp
  * Ronald Portier <rportier@therp.nl>
* Tecnativa (https://www.tecnativa.com)
  * Pedro M. Baeza <pedro.baeza@tecnativa.com>
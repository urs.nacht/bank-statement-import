# Copyright 2014-2017 Akretion (http://www.akretion.com).
# @author Alexis de Lattre <alexis.delattre@akretion.com>
# @author Sébastien BEAU <sebastien.beau@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Import Paypal Bank Statements",
    'summary': 'Import Paypal CSV files as Bank Statements in Odoo',
    "version": "1.0.1.0.0",
    "category": "Accounting",
    "website": "https://gitlab.com/flectra-community/bank-statement-import",
    "author": " Akretion, Flectra Community, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "account_bank_statement_import",
    ],
}

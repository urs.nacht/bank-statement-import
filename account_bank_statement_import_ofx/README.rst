*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
    :alt: License: AGPL-3

=========================
Import OFX Bank Statement
=========================

This module adds support for the import of bank statements in `OFX format <https://en.wikipedia.org/wiki/Open_Financial_Exchange>`_.

Bank Statements may be generated containing a subset of the OFX information (only those transaction lines that are required for the
creation of the Financial Accounting records).

Installation
============

The module requires one additional python lib:

* `ofxparse <http://pypi.python.org/pypi/ofxparse>`_

Usage
=====

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Odoo SA
* Alexis de Lattre <alexis@via.ecp.fr>
* Laurent Mignon <laurent.mignon@acsone.eu>
* Ronald Portier <rportier@therp.nl>
* Sylvain LE GAL <https://twitter.com/legalsylvain>
* Nicolas JEUDY <https://github.com/njeudy>